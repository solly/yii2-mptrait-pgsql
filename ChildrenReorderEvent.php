<?php
/**
 * Created by solly [10.08.16 5:14]
 */

namespace core\mptrait;


use yii\base\Event;
use yii\db\ActiveRecord;

/**
 * Class ChildrenReorderEvent
 *
 * @package core\mptrait
 */
class ChildrenReorderEvent extends Event
{
    /** @var IMaterializedPathModel|ActiveRecord */
    public $parent;
}