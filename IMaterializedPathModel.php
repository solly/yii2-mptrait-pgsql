<?php
/**
 * Created by solly [10.08.16 3:27]
 */
namespace core\mptrait;

use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;


/**
 * Materialized Path interface for Yii2 uses postgres arrays.
 *
 */
interface IMaterializedPathModel
{
    const OPERATION_MAKE_ROOT = 1;
    const OPERATION_PREPEND_TO = 2;
    const OPERATION_APPEND_TO = 3;
    const OPERATION_INSERT_BEFORE = 4;
    const OPERATION_INSERT_AFTER = 5;

    /**
     * Event which fire on changing of children's order.
     */
    const EVENT_CHILDREN_ORDER_CHANGED = 'childrenOrderChanged';

    /****************************************************************************************
     * AttributeAccess
    *****************************************************************************************/

    /**
     * Attribute where path stored (must be as postgres array)
     * return string
    **/
    public function getPathAttribute();

    /**
     * Id attribute
     * return string
     **/
    public function getKeyAttribute();

    /**
     * Attribute for tree root identification
     * return string
     **/
    public function getTreeAttribute();

    /**
     * Attribute for node position
     * return string
     **/
    public function getPositionAttribute();

    /**
     * @return string
    **/
    public static function tableName();
    /**
     * @return bool
    **/
    public function getIsNewRecord();

    /****************************************************************************************
     * Node properties
     *****************************************************************************************/

    /**
     * @return string
     */
    public function getPathColumn();


    /**
     * Returns path of self node.
     *
     * @param bool $asArray = true Return array instead string
     *
     * @return array|string
     */
    public function getPath($asArray = true);

    /**
     * Returns path from root to parent node.
     *
     * @param bool $asArray = true
     *
     * @return null|string|array
     */
    public function getParentPath($asArray = true);

    /**
     * Return Level of self node.
     *
     * @return int
     */
    public function getLevel();

    /**
     * @return bool
     */
    public function isRoot();

    /**
     * @param IMaterializedPathModel $node
     *
     * @return bool
     */
    public function isDescendantOf(IMaterializedPathModel $node);

    /**
     * @param IMaterializedPathModel $node
     *
     * @return bool
     */
    public function isChildOf(IMaterializedPathModel $node);

    /**
     * @return bool
     */
    public function isLeaf();

    /**
     * Returns key of parent.
     *
     * @return mixed|null
     */
    public function getParentKey();

    /**
     * Convert path values from  array to string
     *
     * @param array $path
     * @return string
     */
    public function pathArrayToStr($path);

    /**
     * Convert path values from string to array
     *
     * @param string $path
     *
     * @return array
     */
    public function pathStrToArray($path);

    /****************************************************************************************
     * ActiveQuery
     *****************************************************************************************/
    /**
     * Returns list of parents from root to self node.
     *
     * @param int $depth = null
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParents($depth = null);

    /**
     * Return closest parent.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent();

    /**
     * Returns root node in self node's subtree.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoot();

    /**
     * Returns descendants as plane list.
     *
     * @param int  $depth   = null
     * @param bool $andSelf = false
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescendants($depth = null, $andSelf = false);

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren();

    /**
     * Return previous sibling.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrev();

    /**
     * Return next sibling.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNext();


    /****************************************************************************************
     * ActiveRecord
     *****************************************************************************************/

    /**
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function makeRoot();

    /**
     * @param IMaterializedPathModel|ActiveRecord $node
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function prependTo(IMaterializedPathModel $node);

    /**
     * @param IMaterializedPathModel|ActiveRecord $node
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function appendTo(IMaterializedPathModel $node);

    /**
     * @param IMaterializedPathModel|ActiveRecord $node
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function insertBefore(IMaterializedPathModel $node);

    /**
     * @param IMaterializedPathModel|ActiveRecord $node
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function insertAfter(IMaterializedPathModel $node);

    /**
     * Returns descendants nodes as tree with self node in the root.
     *
     * @param int $depth = null
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function populateTree($depth = null);

    /**
     * Get tree depth level
     *
     * @param IMaterializedPathModel $node
     *
     * @return int
     **/
    public function getTreeDepth(IMaterializedPathModel $node = null);


    /****************************************************************************************
     * Event staff
     *****************************************************************************************/

    /**
     * @param bool insert
     * @throws Exception
     * @throws NotSupportedException
     */
    public function beforeSave($insert);

    /**
     * @param \yii\db\AfterSaveEvent $event
     */
    public function afterInsert($event);

    /**
     * @param \yii\db\AfterSaveEvent $event
     */
    public function afterUpdate($event);

    /**
     * @throws \yii\db\Exception
     */
    public function beforeDelete();

    public function afterDelete();

    public function refresh();

    /**
     * Reorders children with values of $sortAttribute begin from zero.
     *
     * @param bool $inBackground = false Run reordering in single query bypassing models.
     *                           NOTE: position will be not update in models. Only in database.
     *
     * @throws \Exception
     */
    public function reorderChildren($inBackground = true);


    public function onChildrenReorder(ChildrenReorderEvent $event);
}