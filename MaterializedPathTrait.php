<?php
/**
 * Created by solly [10.08.16 3:48]
 */

namespace core\mptrait;

use yii\base\Event;
use yii\base\Exception;
use yii\base\InvalidValueException;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\db\Expression;
use yii\db\Query;

/**
 * @see IMaterializedPathModel
 * @var IMaterializedPathModel|ActiveRecordInterface|ActiveRecord $this
 **/
trait MaterializedPathTrait
{
    /**
     * Step for  position order
     *
     * @var int $step
     **/
    public $step = 100;
    /** @var int */
    protected $operation;
    /** @var ActiveRecord|IMaterializedPathModel */
    protected $node;

    public function init()
    {
        parent::init();
        $this->on(static::EVENT_AFTER_INSERT, [$this, 'afterInsert']);
        $this->on(static::EVENT_AFTER_UPDATE, [$this, 'afterUpdate']);
        $this->on(static::EVENT_CHILDREN_ORDER_CHANGED,[$this,'onChildrenReorder']);
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return $this->pathColumn();
    }

    /** @return  string */
    protected function pathColumn()
    {
        return static::tableName() . "." . $this->getPathAttribute();
    }

    /**
     * Attribute where path stored (must be as postgres array)
     * return string
     **/
    public function getPathAttribute()
    {
        return 'path';
    }

    /**
     * @param  IMaterializedPathModel $node
     *
     * @return bool
     */
    public function isDescendantOf(IMaterializedPathModel $node)
    {
        $key = $node->{$this->getKeyAttribute()};
        $path = $this->getPath();
        $result = in_array($key, $path);
        if ($result && $this->getTreeAttribute() !== null) {
            $result = $this->{$this->getTreeAttribute()} === $node->{$this->getTreeAttribute()};
        }
        return $result;
    }

    /**
     * Id attribute
     * return string
     **/
    public function getKeyAttribute()
    {
        return 'id';
    }

    /**
     * Returns path of self node.
     *
     * @param bool $asArray = true Return array instead string
     *
     * @return array|string
     */
    public function getPath($asArray = true)
    {
        $str = $this->{$this->getPathAttribute()};
        return $asArray ? $this->pathStrToArray($str) : $str;
    }

    /**
     * Convert path values from string to array
     *
     * @param string $path
     *
     * @return array
     */
    public function pathStrToArray($path)
    {
        return explode(",", trim($path, "{}"));
    }

    /**
     * @param IMaterializedPathModel $node
     *
     * @return bool
     */
    public function isChildOf(IMaterializedPathModel $node)
    {
        if ($node->getIsNewRecord()) {
            return false;
        }

        $result = $node->{$this->getKeyAttribute()} == $this->getParentKey();
        if ($result && $this->getTreeAttribute() !== null) {
            $result = $this->{$this->getTreeAttribute()} === $node->{$this->getTreeAttribute()};
        }
        return $result;
    }

    /**
     * Returns key of parent.
     *
     * @return mixed|null
     */
    public function getParentKey()
    {
        if ($this->isRoot()) {
            return null;
        }
        $path = $this->getParentPath();
        return array_pop($path);
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return $this->getLevel() == 1;
    }

    /**
     * Return Level of self node.
     *
     * @return int
     */
    public function getLevel()
    {
        return count($this->getPath());
    }

    /**
     * Returns path from root to parent node.
     *
     * @param bool $asArray = true
     *
     * @return null|string|array
     */
    public function getParentPath($asArray = true)
    {
        if ($this->isRoot()) {
            return null;
        }
        $path = $this->getPath();
        array_pop($path);
        return $asArray ? $path : $this->pathArrayToStr($path);
    }

    /**
     * Convert path values from  array to string
     *
     * @param array $path
     *
     * @return string
     */
    public function pathArrayToStr($path)
    {
        return "{" . implode(",", $path) . "}";
    }

    /**
     * @return bool
     */
    public function isLeaf()
    {
        return count($this->children) === 0;
    }

    /**
     * Returns root node in self node's subtree.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRoot()
    {
        $path = $this->getPath();
        $path = array_shift($path);
        $query = static::find();
        /** @var \yii\db\ActiveQuery $query */
        $query->andWhere([$this->keyColumn() => $path])->andWhere($this->getTreeCondition())->limit(1);
        $query->multiple = false;
        return $query;
    }

    /** @return  string */
    protected function keyColumn()
    {
        return static::tableName() . "." . $this->getKeyAttribute();
    }

    /**
     * @return array
     */
    protected function getTreeCondition()
    {
        return $this->getTreeAttribute() !== null ? [$this->treeColumn() => $this->{$this->getTreeAttribute()}] : [];
    }

    /** @return  string */
    protected function treeColumn()
    {
        return static::tableName() . "." . $this->getTreeAttribute();
    }

    /**
     * Attribute for tree root identification
     * return string
     **/
    public function getTreeAttribute()
    {
        return 'tree';
    }

    /**
     * Return closest parent.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        $query = $this->getParents(1)->limit(1);
        $query->multiple = false;
        return $query;
    }

    /**
     * Returns list of parents from root to self node.
     *
     * @param int $depth = null
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParents($depth = null)
    {
        $path = $this->getParentPath();
        if ($path === null) {
            $path = [];
        } elseif ($depth !== null) {
            $path = array_slice($path, -$depth);
        }

        /** @var \yii\db\ActiveQuery $query */
        $query = static::find();
        $query->andWhere([$this->keyColumn() => $path])->andWhere($this->getTreeCondition())->addOrderBy(
            [$this->pathColumn() => SORT_ASC]
        );
        $query->multiple = true;
        return $query;
    }

    /**
     * Return previous sibling.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrev()
    {
        return $this->getNearestSibling("prev");
    }

    /**
     * @param string $order "prev"|"next"
     *
     * @return \yii\db\ActiveQuery
     * @throws Exception
     */
    protected function getNearestSibling($order)
    {
        $path = $this->getParentPath();
        if ($path === null) {
            return [];
        }
        $keysStr = implode(",", $path);

        /** @var \yii\db\ActiveQuery $query */
        $query = static::find();
        $query->andWhere("{$this->pathColumn()} && array[{$keysStr}]")->andWhere(
            $this->getLevelCondition($this->getLevel())
        )->andWhere($this->getTreeCondition())->limit(1);

        if ($order === "prev") {
            $query->andWhere(['<', $this->positionColumn(), $this->{$this->getPositionAttribute()}])->orderBy(
                [$this->getPositionAttribute() => SORT_DESC]
            );
        } elseif ($order === "next") {
            $query->andWhere(['>', $this->positionColumn(), $this->{$this->getPositionAttribute()}])->orderBy(
                [$this->getPositionAttribute() => SORT_ASC]
            );

        } else {
            throw new Exception("Invalid value of order argument.");
        }
        $query->multiple = false;
        return $query;
    }

    /**
     * @param int    $level
     * @param string $sign
     *
     * @return string
     */
    protected function getLevelCondition($level, $sign = "=")
    {
        return "array_length({$this->pathColumn()}, 1) {$sign} {$level}";
    }

    /** @return  string */
    protected function positionColumn()
    {
        return static::tableName() . "." . $this->getPositionAttribute();
    }

    /**
     * Attribute for node position
     * return string
     **/
    public function getPositionAttribute()
    {
        return 'position';
    }

    /**
     * Return next sibling.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNext()
    {
        return $this->getNearestSibling("next");
    }

    /**
     * @return ActiveRecord|IMaterializedPathModel
     */
    public function makeRoot()
    {
        $this->operation = static::OPERATION_MAKE_ROOT;
        return $this;
    }

    /**
     * @param ActiveRecord|IMaterializedPathModel $node
     *
     * @return ActiveRecord|IMaterializedPathModel
     */
    public function prependTo(IMaterializedPathModel $node)
    {
        $this->operation = static::OPERATION_PREPEND_TO;
        $this->node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord|IMaterializedPathModel $node
     *
     * @return ActiveRecord|IMaterializedPathModel
     */
    public function appendTo(IMaterializedPathModel $node)
    {
        $this->operation = static::OPERATION_APPEND_TO;
        $this->node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord|IMaterializedPathModel $node
     *
     * @return ActiveRecord|IMaterializedPathModel
     */
    public function insertBefore(IMaterializedPathModel $node)
    {
        $this->operation = static::OPERATION_INSERT_BEFORE;
        $this->node = $node;
        return $this;
    }

    /**
     * @param ActiveRecord|IMaterializedPathModel $node
     *
     * @return ActiveRecord|IMaterializedPathModel
     */
    public function insertAfter(IMaterializedPathModel $node)
    {
        $this->operation = static::OPERATION_INSERT_AFTER;
        $this->node = $node;
        return $this;
    }

    /**
     * Get tree depth level
     *
     * @param IMaterializedPathModel $node
     *
     * @throws InvalidValueException
     * @return int
     **/
    public function getTreeDepth(IMaterializedPathModel $node = null)
    {
        if (!$node) {
            $node = $this;
        }
        if (!$node->{$this->getTreeAttribute()}) {
            throw new InvalidValueException('Can`t get tree info for node without treeAttribute');
        }
        $depth = (new Query())->from(static::tableName())->where(
                [$this->getTreeAttribute() => $node->{$this->getTreeAttribute()}]
            )->max("array_length({$this->pathColumn()}, 1)", static::getDb());
        return (int)$depth;
    }

    /**
     * Returns descendants nodes as tree with self node in the root.
     *
     * @param int $depth = null
     *
     * @return IMaterializedPathModel|ActiveRecord
     */
    public function populateTree($depth = null)
    {
        $nodes = $this->getDescendants($depth)->indexBy($this->getKeyAttribute())->all();
        $relates = [];
        foreach ($nodes as $key => $node) {
            $path = $node->getParentPath();
            $parentKey = array_pop($path);
            if (!isset($relates[$parentKey])) {
                $relates[$parentKey] = [];
            }
            $relates[$parentKey][] = $node;
        }
        $nodes[$this->{$this->getKeyAttribute()}] = $this;
        foreach ($relates as $key => $children) {
            $nodes[$key]->populateRelation('children', $children);
        }
        return $this;
    }

    /**
     * Returns descendants as plane list.
     *
     * @param int  $depth   = null
     * @param bool $andSelf = false
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescendants($depth = null, $andSelf = false)
    {
        $keyValue = $this->{$this->getKeyAttribute()};
        /** @var \yii\db\ActiveQuery $query */
        $query = static::find();
        $query->andWhere("{$this->pathColumn()} && array[{$keyValue}]");
        if (!$andSelf) {
            $query->andWhere(["!=", "{$this->keyColumn()}", $keyValue]);
        }
        if ($depth !== null) {
            $maxLevel = $depth + $this->getLevel();
            $query->andWhere($this->getLevelCondition($maxLevel, "<="));
        }
        $query->andWhere($this->getTreeCondition())->addOrderBy(
            [
                "array_length({$this->pathColumn()}, 1)" => SORT_ASC,
                "{$this->positionColumn()}" => SORT_ASC,
                "{$this->keyColumn()}" => SORT_ASC,
            ]
        );
        $query->multiple = true;
        return $query;
    }

    /**
     * @param bool $insert
     *
     * @throws \yii\db\Exception
     * @throws NotSupportedException
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->node !== null && !$this->node->getIsNewRecord()) {
            $this->node->refresh();
        }
        if ($this->getIsNewRecord()) {
            $this->{$this->getPathAttribute()} = $this->pathArrayToStr([]);
        }
        $this->trace($this->getAttributes(), __METHOD__);

        switch ($this->operation) {
            case static::OPERATION_MAKE_ROOT:
                $this->makeRootInternal();
                break;
            case static::OPERATION_PREPEND_TO:
                $this->insertIntoInternal(false);
                break;
            case static::OPERATION_APPEND_TO:
                $this->insertIntoInternal(true);
                break;
            case static::OPERATION_INSERT_BEFORE:
                $this->insertNearInternal(false);
                break;
            case static::OPERATION_INSERT_AFTER:
                $this->insertNearInternal(true);
                break;
            default:
                if ($this->getIsNewRecord()) {
                    throw new NotSupportedException(
                        'Method "' . __METHOD__ . ' is not supported for inserting 
                    new nodes.'
                    );
                }
                $path = $this->getParentPath();
                $path[] = $this->{$this->getKeyAttribute()};
                $this->{$this->getPathAttribute()} = $this->pathArrayToStr($path);
        }
        return parent::beforeSave($insert);
    }

    protected function trace($message, $method)
    {
        \Yii::trace($message, $method);
    }

    /**
     * Make root operation internal handler
     */
    protected function makeRootInternal()
    {
        $this->trace(
            [
                'beforeMakeRoot' => $this->getAttributes(),
                'traitProps' => [
                    $this->getKeyAttribute() => $this->{$this->getKeyAttribute()},
                    $this->getTreeAttribute() => $this->{$this->getTreeAttribute()},
                    $this->getPathAttribute() => $this->{$this->getPathAttribute()},
                    $this->getPositionAttribute() => $this->{$this->getPositionAttribute()},
                    $this->pathColumn(),
                    $this->keyColumn(),
                    $this->treeColumn(),
                    $this->positionColumn(),
                ],
                'dirtyAttrs' => $this->getDirtyAttributes(),
                'isNew' => $this->getIsNewRecord(),
                'pk' => $this->primaryKey,
                'pk()' => $this->getPrimaryKey()
            ],
            __METHOD__
        );
        $key = $this->{$this->getKeyAttribute()};
        $this->{$this->getPathAttribute()} = $key !== null ? $this->pathArrayToStr([$key]) : $this->pathArrayToStr([]);
        if ($this->getPositionAttribute() !== null) {
            $this->{$this->getPositionAttribute()} = 0;
        }
        if ($this->getTreeAttribute() !== null && !$this->getDirtyAttributes([$this->getTreeAttribute()])
            && !$this->getIsNewRecord()
        ) {
            $this->{$this->getTreeAttribute()} = $this->getPrimaryKey();
        }
        $this->trace(['afterMakeRoot' => $this->getAttributes()], __METHOD__);
    }

    /**
     * Append to operation internal handler
     *
     * @param bool $append
     *
     * @throws Exception
     */
    protected function insertIntoInternal($append)
    {
        $this->trace($this->getAttributes(), __METHOD__ . ':' . ($append?'Append':'Prepend'));
        $this->checkNode(false);
        $key = $this->{$this->getKeyAttribute()};
        if ($key !== null) {
            $path = $this->node->getPath();
            $path[] = $key;
            $this->{$this->getPathAttribute()} = $this->pathArrayToStr($path);
        }

        if ($this->getTreeAttribute() !== null) {
            $this->{$this->getTreeAttribute()} = $this->node->{$this->getTreeAttribute()};
        }
        if ($this->getPositionAttribute() !== null) {
            $to = $this->node->getChildren()->orderBy(null);
            $to = $append ? $to->max($this->getPositionAttribute()) : $to->min($this->getPositionAttribute());
            if (!$this->getIsNewRecord() && (int)$to === $this->{$this->getPositionAttribute()}
                && !$this->getDirtyAttributes([$this->getPathAttribute()])
            ) {
            } elseif ($to !== null) {
                $to += $append ? $this->step : -$this->step;
            } else {
                $to = 0;
            }
            $this->{$this->getPositionAttribute()} = $to;
        }
    }

    /**
     * @param bool $forInsertNear
     *
     * @throws Exception
     */
    protected function checkNode($forInsertNear = false)
    {
        if ($forInsertNear && $this->node->isRoot()) {
            throw new Exception('Can not move a node before/after root.');
        }
        if ($this->node->getIsNewRecord()) {
            throw new Exception('Can not move a node when the target node is new record.');
        }
        if ($this->equals($this->node)) {
            throw new Exception('Can not move a node when the target node is same.');
        }
        if ($this->node->isDescendantOf($this)) {
            throw new Exception('Can not move a node when the target node is child.');
        }
    }

    /**
     * Insert operation internal handler
     *
     * @param bool $forward
     *
     * @throws Exception
     */
    protected function insertNearInternal($forward)
    {
        $this->checkNode(true);
        $key = $this->{$this->getKeyAttribute()};
        if ($key !== null) {
            $path = $this->node->getParentPath($this->getPathAttribute());
            $path[] = $key;
            $this->{$this->getPathAttribute()} = $this->pathArrayToStr($path);
        }
        if ($this->getTreeAttribute() !== null) {
            $this->{$this->getTreeAttribute()} = $this->node->{$this->getTreeAttribute()};
        }
        if ($this->getPositionAttribute() !== null) {
            $position = $this->node->{$this->getPositionAttribute()};
            if ($forward) {
                $position++;
            } else {
                $position--;
            }
            $this->{$this->getPositionAttribute()} = $position;
        }
    }

    public function afterInsert($event)
    {
        $this->trace('start', __METHOD__);
        if ($this->operation === static::OPERATION_MAKE_ROOT && $this->getTreeAttribute() !== null
            && $this->{$this->getTreeAttribute()} === null
        ) {
            $key = $this->{$this->getKeyAttribute()};
            $this->{$this->getTreeAttribute()} = $key;
            $this->updateAll([$this->getTreeAttribute() => $key], [$this->getKeyAttribute() => $key]);
        }

        if ($this->{$this->getPathAttribute()} === $this->pathArrayToStr([])) {
            $key = $this->{$this->getKeyAttribute()};
            if ($this->operation === static::OPERATION_MAKE_ROOT) {
                $path = $this->pathArrayToStr([$key]);
            } else {
                if ($this->operation === static::OPERATION_INSERT_BEFORE
                    || $this->operation === static::OPERATION_INSERT_AFTER
                ) {
                    $path = $this->node->getParentPath();
                } else {
                    $path = $this->node->getPath();
                }
                $path[] = $key;
                $path = $this->pathArrayToStr($path);
            }
            $this->{$this->getPathAttribute()} = $path;
            $this->updateAll([$this->getPathAttribute() => $path], [$this->getKeyAttribute() => $key]);
        }

        $this->fireEvent();
        $this->operation = null;
        $this->node = null;
    }

    protected function fireEvent()
    {
        if ($this->operation === static::OPERATION_PREPEND_TO
            || $this->operation === static::OPERATION_APPEND_TO
            || $this->operation === static::OPERATION_INSERT_AFTER
            || $this->operation === static::OPERATION_INSERT_BEFORE
        ) {
            $this->trace('event fired',__METHOD__);
            Event::trigger(
                get_called_class(),
                static::EVENT_CHILDREN_ORDER_CHANGED,
                new ChildrenReorderEvent(
                    [
                        'parent' => $this->parent
                    ]
                )
            );
        }
    }

    /**
     * @param \yii\db\AfterSaveEvent $event
     */
    public function afterUpdate($event)
    {
        $this->moveNode($event->changedAttributes);
        $this->fireEvent();
        $this->operation = null;
        $this->node = null;
    }

    /**
     * @param array $changedAttributes
     *
     * @throws Exception
     */
    protected function moveNode($changedAttributes)
    {
        $oldPath = isset($changedAttributes[$this->getPathAttribute()]) ? $changedAttributes[$this->getPathAttribute()]
            : $this->{$this->getPathAttribute()};
        $update = [];
        $params = [];
        $condition = ['and', "{$this->pathColumn()} && array[{$this->{$this->getKeyAttribute()}}]"];
        if ($this->getTreeAttribute() !== null) {
            $tree = isset($changedAttributes[$this->getTreeAttribute()]) ? $changedAttributes[$this->getTreeAttribute()]
                : $this->{$this->getTreeAttribute()};
            $condition[] = [$this->getTreeAttribute() => $tree];
        }

        if (isset($changedAttributes[$this->getPathAttribute()])) {
            $newParentPath = implode(',', $this->getParentPath(true));
            $oldParentLevel = count($this->pathStrToArray($oldPath));
            $e1 = new Expression("array[{$newParentPath}]");
            $e2 = new Expression(
                "{$this->getPathAttribute()}[{$oldParentLevel}:array_length({$this->getPathAttribute()}, 1)]"
            );
            $update['path'] = new Expression($e1 . "||" . $e2);
            $condition[] = 'path <> :pathNew';

            //$sparams[':pathOld'] = $oldPath;
            $params[':pathNew'] = $this->getAttribute($this->getPathAttribute());
        }
        if ($this->getTreeAttribute() !== null && isset($changedAttributes[$this->getTreeAttribute()])) {
            $update[$this->getTreeAttribute()] = $this->{$this->getTreeAttribute()};
        }
        $this->trace(['beforeMoveNode' => $this->getAttributes()], __METHOD__ . ':3');

        if (!empty($update)) {
            $this->updateAll($update, $condition, $params);
        }

    }

    /**
     * @throws Exception
     */
    public function beforeDelete()
    {
        if ($this->getIsNewRecord()) {
            throw new Exception('Can not delete a node when it is new record.');
        }
        return true;
    }

    public function afterDelete()
    {
        $this->deleteAll($this->getDescendants(null, true)->where);
    }

    /**
     * Reorders children with values of $sortAttribute begin from zero.
     *
     * @param bool $inBackground = false Run reordering in single query bypassing models.
     *                           NOTE: position will be not update in models. Only in database.
     *
     * @throws \Exception
     */
    public function reorderChildren($inBackground = true)
    {
        static::getDb()->transaction(
            function () use ($inBackground) {
                if ($inBackground) {
                    $table = static::tableName();
                    $keyField = $this->getKeyAttribute();
                    $pathField = $this->getPathAttribute();
                    $positionField = $this->getPositionAttribute();
                    $parentKey = $this->$keyField;
                    $step = $this->step;
                    static::getDb()->createCommand(
                        "
                    DO $$
                      DECLARE
                        pos integer := 0;
                        row record;
                      BEGIN
                        FOR row in
                          SELECT {$keyField}
                          FROM {$table}
                          WHERE {$pathField} && array[{$parentKey}] AND {$keyField} != {$parentKey}
                        LOOP
                          UPDATE {$table} set {$positionField} = pos * {$step}
                          WHERE {$keyField} = row.{$keyField};
                          pos := pos + 1;
                        END LOOP;
                    END $$;
                "
                    )->execute();
                } else {
                    foreach ($this->getChildren()->each() as $i => $child) {
                        $child->{$this->getPositionAttribute()} = ($i - 1) * $this->step;
                        $child->save(false, [$this->getPositionAttribute()]);
                    }
                }
            }
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->getDescendants(1);
    }

    public function onChildrenReorder(ChildrenReorderEvent $event)
    {
        $this->trace('Event haldled '.$event->sender->name.'#'.$event->sender->id,__METHOD__);
        $event->parent->reorderChildren(true);
    }

    /**
     * @param int $parentKey
     *
     * @return string
     */
    protected function getChildrenCondition($parentKey)
    {
        return "{$this->pathColumn()} && array[{$parentKey}]";
    }
}