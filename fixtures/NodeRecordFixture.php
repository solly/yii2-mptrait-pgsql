<?php
/**
 * Created by solly [11.08.16 5:30]
 */

namespace core\mptrait\fixtures;


use core\mptrait\models\NodeRecord;
use yii\test\ActiveFixture;

class NodeRecordFixture extends ActiveFixture
{
    public $modelClass = NodeRecord::class;
    public $tableName = '{{%mptrait}}';
    public $dataFile = __DIR__ . '/tree.php';
}