<?php

use yii\db\Migration;

class m160809_191949_nodetree extends Migration
{
    protected $tableName = '{{%mptrait}}';
    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => 'serial primary key',
            'path' => 'int[] not null',
            'position' => 'int not null',
            'tree' => 'int',
            'name' => 'varchar(255) not null',
            'created'=>$this->dateTime(0)
        ]);
    }
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
