<?php
/**
 * Created by solly [10.08.16 5:31]
 */

namespace core\mptrait\models;


use core\mptrait\MaterializedPathQueryTrait;
use yii\db\ActiveQuery;

class NodeQuery extends ActiveQuery
{
    use MaterializedPathQueryTrait;
}