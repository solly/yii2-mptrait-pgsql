<?php
/**
 * Created by solly [10.08.16 5:18]
 */

namespace core\mptrait\models;


use core\mptrait\IMaterializedPathModel;
use core\mptrait\MaterializedPathTrait;
use yii\db\ActiveRecord;

/**
 * @package core\mptrait\models
 *
 * @property int          $id
 * @property array        $path
 * @property int          $position
 * @property int          $tree
 * @property string       $name
 * @property string       $created
 *
 * @property NodeRecord   $parent
 * @property NodeRecord   $root
 * @property NodeRecord   $prev
 * @property NodeRecord   $next
 * @property NodeRecord[] $children
 **/
class NodeRecord extends ActiveRecord implements IMaterializedPathModel
{
    use MaterializedPathTrait {
        MaterializedPathTrait::beforeSave as beforeSaveTrait;
    }

    public static function tableName()
    {
        return '{{%mptrait}}';
    }

    /**
     * {@inheritdoc}
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 200, 'min' => 3]
        ];
    }

    public static function find()
    {
        return new NodeQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->created = date('Y-m-d H:i:s');
            $this->beforeSaveTrait($insert);
        }
        return true;
    }

}