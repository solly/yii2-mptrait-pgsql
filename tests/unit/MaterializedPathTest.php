<?php

namespace core\mptrait\tests\unit;

use core\mptrait\IMaterializedPathModel;
use core\mptrait\models\NodeRecord;
use yii\base\Event;
use yii\helpers\ArrayHelper;

class MaterializedPathTest extends DbTestCase
{
    public function setUp()
    {
        parent::setUp();
        NodeRecord::deleteAll();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testMakeRoot()
    {
        $node = new NodeRecord(['name' => 'test name']);
        $node = $node->makeRoot();
        $this->assertTrue($node->validate());
        $res = $node->save();
        \Yii::trace(['res' => $res, 'Validation' => $node->getErrors(), 'node' => $node->getAttributes()], __METHOD__);

        $this->assertTrue($res);

        $this->assertTrue($node->isRoot());
        $this->assertEquals($node->getLevel(), 1);
        $this->assertEquals(NodeRecord::find()->count(), 1);

        $records = NodeRecord::find()->roots()->all();
        $this->assertEquals(count($records), 1);
        $this->assertEquals($records[0]->id, $node->id);
    }

    /**
     * @depends testMakeRoot
     **/
    public function testReorderChildren()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->prependTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $node2->prependTo($root)->save();

        $node3 = new NodeRecord(['name' => 'node 3']);
        $node3->prependTo($root)->save();

        $root->reorderChildren(true);

        $i = 0;
        foreach ($root->getChildren()->each() as $record) {
            \Yii::trace($record->getAttributes(), __METHOD__);

            $this->assertEquals($record->position, 100 * $i++);
        }
    }

    /**
     * @depends testMakeRoot
     **/
    public function testGetParent()
    {
        $root = new NodeRecord(['name' => 'root']);
        $this->assertTrue($root->makeRoot()->save());

        $node = new NodeRecord(['name' => 'node']);
        $this->assertTrue($node->appendTo($root)->save());

        $this->assertEquals($node->parent->id, $root->id);
        \Yii::trace($node, __METHOD__);

    }

    /**
     * @depends testMakeRoot
     **/
    public function testGetParents()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $this->assertTrue($node2->appendTo($node1)->save());

        $parents = $node2->parents;
        $this->assertEquals(2, count($parents));
        $this->assertEquals($root->id, $parents[0]->id);
        $this->assertEquals($node1->id, $parents[1]->id);
        \Yii::trace($parents, __METHOD__);

    }

    /**
     * @depends testMakeRoot
     **/
    public function testAppendTo()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $this->assertTrue($node2->appendTo($root)->save());

        $this->assertGreaterThan($node1->position, $node2->position);
    }

    /**
     * @depends testMakeRoot
     **/
    public function testPrependTo()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $this->assertTrue($node2->prependTo($root)->save());

        $this->assertLessThan($node1->position, $node2->position);
    }

    /**
     * @depends testMakeRoot
     **/
    public function testInsertBefore()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $node2->insertBefore($node1)->save();

        $this->assertEquals($node2->parent->id, $root->id);
        $this->assertLessThan($node1->position, $node2->position);
    }

    /**
     * @depends testMakeRoot
     **/
    public function testInsertAfter()
    {
        $root = new NodeRecord(['name' => 'root']);
        $root->makeRoot()->save();

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());

        $node2 = new NodeRecord(['name' => 'node 2']);
        $node2->appendTo($root)->save();

        $node3 = new NodeRecord(['name' => 'node 3']);
        $node3->insertAfter($node1)->save();

        $this->assertEquals($node3->parent->id, $root->id);
        $this->assertGreaterThan($node1->position, $node3->position);
        $this->assertGreaterThan($node3->position, $node2->position);
    }

    /**
     * @depends testMakeRoot
     **/
    public function testEventFiring()
    {
        $root = new NodeRecord(['name' => 'root']);
        $this->assertTrue($root->makeRoot()->save());

        $eventParentId = null;
        Event::on(
            NodeRecord::class,
            IMaterializedPathModel::EVENT_CHILDREN_ORDER_CHANGED,
            function ($event) use (&$eventParentId) {
                $eventParentId = $event->parent->id;
            }
        );

        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());
        $this->assertEquals($eventParentId, $root->id);

        $node2 = new NodeRecord(['name' => 'node 2']);
        $this->assertTrue($node2->prependTo($root)->save());
        $this->assertEquals($eventParentId, $root->id);
        $eventParentId = null;

        $node3 = new NodeRecord(['name' => 'node 3']);
        $this->assertTrue($node3->insertBefore($node1)->save());
        $this->assertEquals($eventParentId, $root->id);
        $eventParentId = null;

        $node4 = new NodeRecord(['name' => 'node 4']);
        $this->assertTrue($node4->insertAfter($node1)->save());
        $this->assertEquals($eventParentId, $root->id);
        $eventParentId = null;
    }

    public function testTreeDepth(){
        $root = new NodeRecord(['name' => 'root']);
        $this->assertTrue($root->makeRoot()->save());
        $depth = $root->getTreeDepth();
        $this->assertEquals(1, $depth);
        $node1 = new NodeRecord(['name' => 'node 1']);
        $this->assertTrue($node1->appendTo($root)->save());
        $depth = $node1->getTreeDepth();
        $this->assertEquals(2,$depth);
        $this->assertEquals($node1->getTreeDepth($root),$root->getTreeDepth());

        $root2 = new NodeRecord(['name' => 'root2']);
        $this->assertTrue($root2->makeRoot()->save());

        $this->assertEquals(1,$root2->getTreeDepth());
        $node21 = new NodeRecord(['name' => 'node 21']);
        $this->assertTrue($node21->appendTo($root2)->save());
        $node31 = new NodeRecord(['name' => 'node 31']);
        $this->assertTrue($node31->appendTo($node21)->save());
        $this->assertEquals(3,$root2->getTreeDepth());
        $this->assertEquals(3,$node31->getTreeDepth());
        $this->assertEquals(3,$root->getTreeDepth($node21));
        $this->assertEquals(2,$root2->getTreeDepth($node1));
        $this->assertEquals(2,$root2->getTreeDepth($root));
        $node1->delete();
        $this->assertEquals(1,$root2->getTreeDepth($root));

    }

    /**
     * @depends testTreeDepth
    **/
    public function testTreeExperiments()
    {
        $root1 = new NodeRecord(['name' => 'root1']);
        $this->assertTrue($root1->makeRoot()->save());
        $this->makeTree($root1, 'root1sub');
        $root2 = new NodeRecord(['name' => 'root2']);
        $this->assertTrue($root2->makeRoot()->save());
        $this->makeTree($root2, 'root2sub');
        $root3 = new NodeRecord(['name' => 'root3']);
        $this->assertTrue($root3->makeRoot()->save());
        $this->makeTree($root3, 'root3sub');

        $roots = NodeRecord::find()->roots()->asArray()->all();
        \Yii::trace(['roots' => $roots], __METHOD__);
        $this->assertEquals(3, count($roots));
        \Yii::beginProfile('simple find');
        $secondRoot = NodeRecord::find()->where(['name' => 'root2'])->one();
        \Yii::endProfile('simple find');

        $this->assertEquals($root2->id, $secondRoot->id);
        \Yii::beginProfile('get relation');
        $this->assertNotEmpty($secondRoot->descendants);
        \Yii::endProfile('get relation');
    }

    protected function makeTree(NodeRecord $root, $basename)
    {
        for ($i = 0; $i < 3; $i++) {
            $node = new NodeRecord(['name' => $basename . '-L1.' . $i]);
            $this->assertTrue($node->appendTo($root)->save());
            for ($c = 0; $c < 4; $c++) {
                $subnode = new NodeRecord(['name' => $basename . '-L2 -' . $i . '.' . $c]);
                $this->assertTrue($subnode->appendTo($node)->save());
            }
        }
    }
}