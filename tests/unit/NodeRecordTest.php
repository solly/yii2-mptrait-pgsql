<?php
/**
 * Created by solly [11.08.16 5:34]
 */

namespace core\mptrait\tests\unit;


use Codeception\Specify;
use Codeception\Util\Debug;
use Codeception\Verify;
use core\mptrait\fixtures\NodeRecordFixture;
use core\mptrait\models\NodeRecord;

/**
 * @var Verify
 **/
class NodeRecordTest extends DbTestCase
{
    use Specify;

    public function setUp()
    {
        parent::setUp();
        //NodeRecord::deleteAll();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function fixtures()
    {
        return [
            'tree' => NodeRecordFixture::class
        ];
    }

    public function testTree()
    {
        $total = NodeRecord::find()->count();
        verify($total)->equals(13);
        $root = NodeRecord::findOne(1);
        $root->reorderChildren();
        $tree = $root->populateTree();
        verify(sizeof($root->children))->equals(4);
        verify($root->getTreeDepth())->equals(4);
        Debug::debug($this->expandChildren($tree));
    }

    protected function expandChildren(NodeRecord $item)
    {
        $str = str_repeat('-', $item->getLevel() - 1) . ' [' . $item->position . '] ' . $item->name . ' ' . implode('/',
                                                                                                                    $item->getPath(
                                                                                                                    )
            ) . ' ' . PHP_EOL;
        if (!empty($item->children)) {
            foreach ($item->children as $ch) {
                $str .= $this->expandChildren($ch);
            }
        }
        return $str;
    }

    public function testNodeMove()
    {
        //$this->markTestSkipped();
        $root = NodeRecord::findOne(1);


        $this->specify(
            'test prependTo',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_3_6']);
                $targetNode = NodeRecord::findOne(['name' => 'node1_5_13']);
                verify_not($movedNode->isChildOf($targetNode));
                verify_that($movedNode->prependTo($targetNode)->save());
                verify_that($movedNode->isChildOf($targetNode));
                verify($movedNode->getPath())->equals([1, 5, 13, 6]);
                verify($movedNode->getLevel())->equals(4);
                $movedNode->refresh();
                Debug::debug($movedNode->getAttributes());
                Debug::debug($movedNode->getPath());
            }
        );

        $root->refresh();
        Debug::debug($this->expandChildren($root->populateTree()));

        $this->specify(
            'test appendTo',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_7_9_10']);
                $targetNode = NodeRecord::findOne(['name' => 'node1_5_13']);
                verify_not($movedNode->isChildOf($targetNode));
                verify_that($movedNode->appendTo($targetNode)->save());
                verify_that($movedNode->isChildOf($targetNode));
                verify($movedNode->getPath())->equals([1, 5, 13, 10]);
            }
        );

        $this->specify(
            'test appendTo with nested',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_2_4']);
                $targetNode = NodeRecord::findOne(['name' => 'node1_3']);
                verify_not($movedNode->isChildOf($targetNode));
                verify($movedNode->children)->notEmpty();
                verify_that($movedNode->appendTo($targetNode)->save());
                verify_that($movedNode->isChildOf($targetNode));
                verify($movedNode->children)->notEmpty();
                verify($movedNode->getPath())->equals([1, 3, 4]);
            }
        );
        $root = NodeRecord::findOne(1);
        $root->reorderChildren(true);
        Debug::debug($this->expandChildren($root->populateTree()));

        $this->specify(
            'test insertBefore on same level',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_7_9_10']);
                verify_that($movedNode);
                $prev = $movedNode->prev;
                verify_that($prev);
                verify($movedNode->parent->id)->equals($prev->parent->id);
                verify($prev->name)->equals('node1_3_6');
                verify($prev->position)->lessThan($movedNode->position);
                verify_that($movedNode->insertBefore($prev)->save());
                $movedNode->refresh();
                $next = $movedNode->next;
                verify($next->id)->equals($prev->id);
                verify($next->position)->greaterThan($movedNode->position);
            }
        );
        Debug::debug($this->expandChildren($root->populateTree()));
        $this->specify(
            'test insertBefore to other branch',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_7_9_10']);
                $targetNode = NodeRecord::findOne(['name' => 'node1_2_4']);
                verify($movedNode->parent->id)->notEquals($targetNode->parent->id);
                verify_that($movedNode->insertBefore($targetNode)->save());
                $movedNode->refresh();
                $targetNode->refresh();
                verify($movedNode->parent->id)->equals($targetNode->parent->id);
                verify($movedNode->position)->lessThan($targetNode->position);
            }
        );
        Debug::debug($this->expandChildren($root->populateTree()));
        $this->specify(
            'test insertAfter',
            function () {
                $movedNode = NodeRecord::findOne(['name' => 'node1_3_6']);
                $targetNode = NodeRecord::findOne(['name' => 'node1_5_13']);
                verify($movedNode->parent->id)->notEquals($targetNode->parent->id);
                verify($targetNode->getLevel())->lessThan($movedNode->getLevel());
                verify_that($movedNode->insertAfter($targetNode)->save());
                $movedNode->refresh();
                $targetNode->refresh();
                verify($movedNode->parent->id)->equals($targetNode->parent->id);
                verify($movedNode->position)->greaterThan($targetNode->position);
                verify($targetNode->getLevel())->equals($movedNode->getLevel());

            }
        );
        Debug::debug($this->expandChildren($root->populateTree()));

    }

    public function testNodeUpdateDelete()
    {
        $node = NodeRecord::findOne(['name' => 'node1_3_6']);
        $node->name = 'Qwerty';
        verify_that($node->save());
        $node->refresh();
        verify($node->name)->equals('Qwerty');
        $node = NodeRecord::findOne(['name' => 'node1_3']);
        $childrens = $node->children;
        verify_that($childrens);
        verify_that(is_array($childrens));
        $node->delete();
        verify_not(NodeRecord::findOne(['name' => 'node1_3']));
        foreach ($childrens as $child) {
            verify_not(NodeRecord::findOne(['name' => $child->name]));
        }

    }
}