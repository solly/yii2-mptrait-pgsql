<?php

return [
    'id' => 'solly/yii2-mptrait-pgsql',
    'basePath' => __DIR__ . '/../../',
    'bootstrap'=>['log'],
    'components' => [
        'db' => [
            'class' => \yii\db\Connection::class,
            'dsn' => 'pgsql:host=localhost;port=5432;dbname=ptrack_test',
            'username' => 'lusik',
            'password' => 'password',
            'charset' => 'utf8',
            'tablePrefix'=>'itt_',
        ],
        'log'=>[
            'traceLevel' => 3,
            'targets' => [
                [
                    //'class' => \yii\log\SyslogTarget::class,
                    'class' => \core\loggy\components\SyslogTarget::class,
                    'identity'=>'mptraittest',
                    'levels' => ['error', 'trace', 'profile'],
                    'categories'=>['core\mptrait*','yii\db\Exception*']
                ],
            ],
        ]
    ],
];